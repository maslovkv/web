
// this file has the baseline default parameters
{
  components: {
    website: {
      name: 'web',
      image: 'maslovkv/nginx:latest',
      replicas: 2,
      containerPort: 80,
      servicePort: 80,
      nodeSelector: {},
      tolerations: [],
      ingressClass: 'nginx',
      domain: 'web.mkv.org',
    },
  },
}
